from random import randint
from django.db import models
from django.utils.text import slugify
from image_cropping import ImageRatioField
from django.shortcuts import resolve_url as r
from django.db.models.aggregates import Count


UF_SIGLA = [
    ('AC', 'AC'),
    ('AL', 'AL'),
    ('AP', 'AP'),
    ('AM', 'AM'),
    ('BA', 'BA'),
    ('CE', 'CE'),
    ('DF', 'DF'),
    ('ES', 'ES'),
    ('EX', 'EX'),
    ('GO', 'GO'),
    ('MA', 'MA'),
    ('MT', 'MT'),
    ('MS', 'MS'),
    ('MG', 'MG'),
    ('PA', 'PA'),
    ('PB', 'PB'),
    ('PR', 'PR'),
    ('PE', 'PE'),
    ('PI', 'PI'),
    ('RJ', 'RJ'),
    ('RN', 'RN'),
    ('RS', 'RS'),
    ('RO', 'RO'),
    ('RR', 'RR'),
    ('SC', 'SC'),
    ('SP', 'SP'),
    ('SE', 'SE'),
    ('TO', 'TO'),
]

PAGINAS = [
    ('1', 'HOME'),
    ('2', 'SOBRE NÓS'),
    ('3', 'ÁREAS DE ATUAÇÃO'),
    ('4', 'CLIENTES'),
    ('5', 'ARTIGOS'),
    ('6', 'CONTATO'),
    ('7', 'PROFISSIONAIS'),
]

SLIDER_TIPO = [
    ('1', 'HOME'),
    ('2', 'EQUIPE'),
]

def get_upload_pagina_estatica(instance, filename):
    return 'paginas-estaticas/%s/thumb/%s/' % (instance.titulo, filename)


def get_upload_areas_atuacao(instance, filename):
    return 'areas-atuacao/%s/thumb/%s/' % (instance.slug, filename)


def get_upload_equipe(instance, filename):
    return 'equipes/%s/thumb/%s/' % (instance.slug, filename)


def get_upload_cliente(instance, filename):
    return 'clientes/%s/thumb/%s/' % (instance.slug, filename)


def get_upload_artigos(instance, filename):
    return 'artigos/thumb/%s/' % filename


class Configuracao(models.Model):
    titulo = models.CharField(max_length=255, blank=False)
    endereco = models.CharField(max_length=255, blank=True)
    numero = models.CharField(max_length=10, blank=True)
    complemento = models.CharField(max_length=100, blank=True)
    cep = models.CharField(max_length=20, blank=True)
    cidade = models.CharField(max_length=155, blank=True)
    uf = models.CharField(max_length=2, blank=True)
    cod_analytics = models.CharField(blank=True, max_length=255, verbose_name="Código Analytics")
    cod_gmaps = models.TextField(blank=True, verbose_name="Embedded Mapa Google Maps")
    link_gmaps = models.TextField(blank=True, verbose_name="Link para Google Maps")
    fone = models.CharField(max_length=30, blank=True, verbose_name="Telefone")
    email = models.CharField(max_length=100, blank=True)
    facebook = models.CharField(max_length=255, blank=True, null=True)
    instagram = models.CharField(max_length=255, blank=True, null=True)
    youtube = models.CharField(max_length=255, blank=True, null=True)
    linkedin = models.CharField(max_length=255, blank=True, null=True)
    ativo = models.BooleanField(default=False)

    def __str__(self):
        return self.titulo

    class Meta:
        verbose_name = 'Configuração'
        verbose_name_plural = 'Configurações'


class Slider(models.Model):
    titulo = models.CharField(max_length=255, blank=False, null=False)
    subtitulo = models.CharField(max_length=255, blank=True)
    link_botao = models.CharField(max_length=255, blank=True)
    tipo = models.CharField(max_length=100, choices=SLIDER_TIPO, default='HOME')
    image = models.ImageField(upload_to='slider', blank=False, null=False, default=None)
    cropping = ImageRatioField('image', '1920x1057')

    def __str__(self):
        s = u'%s - %s' % (self.titulo, self.subtitulo)
        return s

    def image_tag(self):
        return u'<img src="%s" width="200px" />' % self.image.url
    image_tag.short_description = 'Imagem Atual'
    image_tag.allow_tags = True


class PaginaEstatica(models.Model):
    titulo = models.CharField(max_length=255, blank=False)
    texto = models.TextField(blank=True)
    tipo = models.CharField(max_length=100, choices=PAGINAS)
    image = models.ImageField(blank=True, upload_to=get_upload_pagina_estatica)
    cropping = ImageRatioField('image', '800x800')
    # -------------------------------------------------------- #
    seo_title = models.CharField(max_length=255, blank=True, null=True)
    seo_description = models.TextField(blank=True, null=True)
    seo_keywords = models.TextField(blank=True, null=True)
    # -------------------------------------------------------- #

    def __str__(self):
        return self.titulo

    class Meta:
        verbose_name = 'Página Estática'
        verbose_name_plural = 'Páginas Estáticas'


class AreasAtuacao(models.Model):
    titulo = models.CharField(max_length=255, blank=False)
    texto = models.TextField(blank=True)
    image = models.ImageField(blank=True, upload_to=get_upload_areas_atuacao)
    cropping = ImageRatioField('image', '900x600')
    slug = models.SlugField('slug', max_length=100, blank=True, null=True)
    # -------------------------------------------------------- #
    seo_title = models.CharField(max_length=255, blank=True, null=True)
    seo_description = models.TextField(blank=True, null=True)
    seo_keywords = models.TextField(blank=True, null=True)
    # -------------------------------------------------------- #

    def __str__(self):
        return self.titulo

    def get_absolute_url(self):
        return r('area_detalhe', slug=self.slug)

    def _get_unique_slug(self):
        slug = slugify(self.titulo)
        unique_slug = slug
        num = 1
        while AreasAtuacao.objects.filter(slug=unique_slug).exists():
            unique_slug = '{}-{}'.format(slug, num)
            num += 1
        return unique_slug

    def save(self, *args, **kwargs):
        try:
            this = AreasAtuacao.objects.get(id=self.id)
            if this.image != self.image:
                this.image.delete(save=False)
        except:
            pass  # when new photo then we do nothing, normal case

        if not self.slug:
            self.slug = self._get_unique_slug()

        super().save(*args, **kwargs)

    def random(self):
        count = self.aggregate(count=Count('id'))['count']
        random_index = randint(0, count - 1)
        return self.all()[random_index]

    def image_tag(self):
        foto1 = " "
        if self.image:
            foto1 = self.image.url
        return u'<img width="200px" src="%s" />' % foto1
    image_tag.short_description = 'Imagem Atual'
    image_tag.allow_tags = True

    class Meta:
        verbose_name = 'Área de Atuação'
        verbose_name_plural = 'Áreas de Atuação'


class Equipe(models.Model):
    titulo = models.CharField(max_length=255, blank=False)
    texto = models.TextField(blank=True)
    image = models.ImageField(upload_to=get_upload_equipe, blank=False, null=False, default=None)
    cropping = ImageRatioField('image', '410x535')
    facebook = models.CharField(max_length=255, blank=True, null=True)
    instagram = models.CharField(max_length=255, blank=True, null=True)
    linkedin = models.CharField(max_length=255, blank=True, null=True)
    order = models.PositiveIntegerField()
    slug = models.SlugField('slug', max_length=100, blank=True, null=True)
    # -------------------------------------------------------- #
    seo_title = models.CharField(max_length=255, blank=True, null=True)
    seo_description = models.TextField(blank=True, null=True)
    seo_keywords = models.TextField(blank=True, null=True)
    # -------------------------------------------------------- #

    def __str__(self):
        return self.titulo

    def get_absolute_url(self):
        return r('equipe_detalhe', slug=self.slug)

    def _get_unique_slug(self):
        slug = slugify(self.titulo)
        unique_slug = slug
        num = 1
        while Equipe.objects.filter(slug=unique_slug).exists():
            unique_slug = '{}-{}'.format(slug, num)
            num += 1
        return unique_slug

    def save(self, *args, **kwargs):
        try:
            this = Equipe.objects.get(id=self.id)
            if this.image != self.image:
                this.image.delete(save=False)
        except:
            pass  # when new photo then we do nothing, normal case

        if not self.slug:
            self.slug = self._get_unique_slug()

        super().save(*args, **kwargs)

    def random(self):
        count = self.aggregate(count=Count('id'))['count']
        random_index = randint(0, count - 1)
        return self.all()[random_index]

    def image_tag(self):
        foto1 = " "
        if self.image:
            foto1 = self.image.url
        return u'<img width="200px" src="%s" />' % foto1
    image_tag.short_description = 'Imagem Atual'
    image_tag.allow_tags = True


class Clientes(models.Model):
    titulo = models.CharField(max_length=255, blank=False)
    texto = models.TextField(blank=True)
    image = models.ImageField(blank=True, upload_to=get_upload_cliente)
    cropping = ImageRatioField('image', '1920x530')
    slug = models.SlugField('slug', max_length=100, blank=True, null=True)
    # -------------------------------------------------------- #
    seo_title = models.CharField(max_length=255, blank=True, null=True)
    seo_description = models.TextField(blank=True, null=True)
    seo_keywords = models.TextField(blank=True, null=True)
    # -------------------------------------------------------- #

    def __str__(self):
        return self.titulo

    def get_absolute_url(self):
        return r('clientes_detalhe', slug=self.slug)

    def _get_unique_slug(self):
        slug = slugify(self.titulo)
        unique_slug = slug
        num = 1
        while Clientes.objects.filter(slug=unique_slug).exists():
            unique_slug = '{}-{}'.format(slug, num)
            num += 1
        return unique_slug

    def save(self, *args, **kwargs):
        try:
            this = AreasAtuacao.objects.get(id=self.id)
            if this.image != self.image:
                this.image.delete(save=False)
        except:
            pass  # when new photo then we do nothing, normal case

        if not self.slug:
            self.slug = self._get_unique_slug()

        super().save(*args, **kwargs)

    def random(self):
        count = self.aggregate(count=Count('id'))['count']
        random_index = randint(0, count - 1)
        return self.all()[random_index]


class Artigo(models.Model):
    titulo = models.CharField(max_length=255, blank=False)
    sub_titulo = models.CharField(max_length=255, blank=True)
    texto = models.TextField(blank=True)
    image = models.ImageField(upload_to=get_upload_artigos, blank=False, null=False, default=None)
    cropping = ImageRatioField('image', '370x340')
    data_publicacao = models.DateTimeField(blank=False, null=False)
    slug = models.SlugField('slug', max_length=100, blank=True, null=True)
    # -------------------------------------------------------- #
    seo_title = models.CharField(max_length=255, blank=True, null=True)
    seo_description = models.TextField(blank=True, null=True)
    seo_keywords = models.TextField(blank=True, null=True)
    # -------------------------------------------------------- #

    def __str__(self):
        return self.titulo

    def get_absolute_url(self):
        return r('artigo_detalhe', slug=self.slug)

    def _get_unique_slug(self):
        slug = slugify(self.titulo)
        unique_slug = slug
        num = 1
        while Artigo.objects.filter(slug=unique_slug).exists():
            unique_slug = '{}-{}'.format(slug, num)
            num += 1
        return unique_slug

    def save(self, *args, **kwargs):
        try:
            this = Artigo.objects.get(id=self.id)
            if this.image != self.image:
                this.image.delete(save=False)
        except:
            pass  # when new photo then we do nothing, normal case

        if not self.slug:
            self.slug = self._get_unique_slug()

        super().save(*args, **kwargs)

    def random(self):
        count = self.aggregate(count=Count('id'))['count']
        random_index = randint(0, count - 1)
        return self.all()[random_index]

    def image_tag(self):
        foto1 = " "
        if self.image:
            foto1 = self.image.url
        return u'<img width="200px" src="%s" />' % foto1
    image_tag.short_description = 'Imagem Atual'
    image_tag.allow_tags = True
