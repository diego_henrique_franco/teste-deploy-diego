from .models import Configuracao, PaginaEstatica, Equipe, AreasAtuacao, Equipe, Artigo, Slider, Clientes
from django.contrib import admin
from django.forms import ModelForm
from suit.admin import SortableModelAdmin
from image_cropping import ImageCroppingMixin
from suit_ckeditor.widgets import CKEditorWidget


# Register your models here.
class PaginaEstaticaForm(ModelForm):
    class Meta:
        widgets = {
            'texto': CKEditorWidget(editor_options={'startupFocus': True})
        }


class PaginaEstaticaAdmin(ImageCroppingMixin, admin.ModelAdmin):
    form = PaginaEstaticaForm
    list_display = ('tipo', 'titulo')
    fieldsets = [
        ('', {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': ['titulo', 'texto', 'tipo', 'image', 'cropping']
        }),
        ('', {
                'classes': ('suit-tab', 'suit-tab-seo',),
                'fields': ['seo_title', 'seo_description', 'seo_keywords']
        })

    ]
    suit_form_tabs = (('general', 'Página'), ('seo', 'SEO'))


class AreasAtuacaoForm(ModelForm):
    class Meta:
        widgets = {
            'texto': CKEditorWidget(editor_options={'startupFocus': True})
        }


class AreasAtuacaoAdmin(ImageCroppingMixin, admin.ModelAdmin):
    form = PaginaEstaticaForm
    list_display = ('titulo', 'image_tag')
    fieldsets = [
        ('', {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': ['titulo', 'texto', 'image', 'cropping']
        }),
        ('', {
            'classes': ('suit-tab', 'suit-tab-seo',),
            'fields': ['seo_title', 'seo_description', 'seo_keywords']
        })

    ]
    suit_form_tabs = (('general', 'Dados Gerais'), ('seo', 'SEO'))


# Register your models here.
class ClassificacaoForm(ModelForm):
    class Meta:
        widgets = {
            'texto': CKEditorWidget(editor_options={'startupFocus': True})
        }


class ClassificacaoAdmin(ImageCroppingMixin, admin.ModelAdmin):
    form = ClassificacaoForm
    fieldsets = [
        ('', {
            'classes': ('suit-tab', 'suit-tab-classificacao'),
            'fields': ['titulo', 'texto', 'image', 'cropping', 'slug']
        }),
        ('', {
            'classes': ('suit-tab', 'suit-tab-seo',),
            'fields': ['seo_title', 'seo_description', 'seo_keywords']
        }),
    ]
    suit_form_tabs = (('classificacao', 'Classificação'), ('seo', 'SEO'))


class NoticiaForm(ModelForm):
    class Meta:
        widgets = {
            'texto': CKEditorWidget(editor_options={'startupFocus': True})
        }


class ImpactoDorAdmin(ImageCroppingMixin, admin.ModelAdmin):
    form = PaginaEstaticaForm
    list_display = ('titulo', 'image_tag', 'data_publicacao', 'slug')
    search_fields = ('titulo',)
    fieldsets = [
        ('', {
            'classes': ('suit-tab', 'suit-tab-impactos'),
            'fields': ['titulo', 'texto_linha1', 'texto_linha2', 'texto', 'image', 'cropping', 'data_publicacao', 'slug']
        }),
        ('', {
                'classes': ('suit-tab', 'suit-tab-seo',),
                'fields': ['seo_title', 'seo_description', 'seo_keywords']
        }),
    ]
    suit_form_tabs = (('impactos', 'Impactos'), ('seo', 'SEO'))


class NoticiaForm(ModelForm):
    class Meta:
        widgets = {
            'texto': CKEditorWidget(editor_options={'startupFocus': True})
        }


class NoticiaAdmin(ImageCroppingMixin, admin.ModelAdmin):
    form = NoticiaForm
    list_display = ('titulo', 'sub_titulo', 'image', 'data_publicacao', 'slug')
fieldsets = [
    ('Notícia', {
        'classes': ('full-width',),
        'fields': ('texto')
    })
]


class ArtigoForm(ModelForm):
    class Meta:
        widgets = {
            'texto': CKEditorWidget(editor_options={'startupFocus': True})
        }


class ArtigoAdmin(ImageCroppingMixin, admin.ModelAdmin):
    form = ArtigoForm
    list_display = ('titulo', 'sub_titulo', 'image_tag', 'data_publicacao', 'slug')
    search_fields = ('titulo',)

    fieldsets = [
        ('', {
            'classes': ('suit-tab', 'suit-tab-artigo'),
            'fields': ['titulo', 'sub_titulo', 'texto', 'image', 'cropping', 'data_publicacao', 'slug']
        }),
        ('', {
                'classes': ('suit-tab', 'suit-tab-seo',),
                'fields': ['seo_title', 'seo_description', 'seo_keywords']
        }),
    ]

    suit_form_tabs = (('artigo', 'Artigo'), ('seo', 'SEO'))


class EquipeForm(ModelForm):
    class Meta:
        widgets = {
            'texto': CKEditorWidget(editor_options={'startupFocus': True})
        }


class EquipeAdmin(ImageCroppingMixin, SortableModelAdmin):
    form = EquipeForm
    sortable = 'order'
    list_display = ('titulo', 'image_tag')
    search_fields = ('titulo',)
    fieldsets = [
        ('', {
                'classes': ('suit-tab', 'suit-tab-equipe'),
                'fields': ['titulo', 'texto', 'image', 'cropping', 'facebook', 'instagram', 'linkedin', 'slug']
        }),
        ('', {
                'classes': ('suit-tab', 'suit-tab-seo',),
                'fields': ['seo_title', 'seo_description', 'seo_keywords']
        }),
    ]
    suit_form_tabs = (('equipe', 'Equipe'), ('seo', 'SEO'))


class SliderAdmin(ImageCroppingMixin, admin.ModelAdmin):
    list_display = ('titulo', 'image_tag')


admin.site.register(PaginaEstatica, PaginaEstaticaAdmin)
admin.site.register(Clientes)
admin.site.register(AreasAtuacao, AreasAtuacaoAdmin)
admin.site.register(Artigo,  ArtigoAdmin)
admin.site.register(Equipe, EquipeAdmin)
admin.site.register(Configuracao)
admin.site.register(Slider, SliderAdmin)