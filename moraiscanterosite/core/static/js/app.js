$(document).ready(function () {

    menuToggle();

    // slider
    $('.carousel').carousel({
        ride: true,
        interval: 5000
    })

    // lightbox
    $(document).on('click', '[data-toggle="lightbox"]', function (event) {
        event.preventDefault();
        $(this).ekkoLightbox();
    });

    /*
    $('#heading1').addClass('active-acc');
    $('.collapse').on('shown.bs.collapse', function () {
        $(this).prev().addClass('active-acc');
    });
    $('.collapse').on('hidden.bs.collapse', function () {
        $(this).prev().removeClass('active-acc');
    }); */

});

function menuToggle() {
    $('.menu-plate').on('click', function (e) {
        e.stopPropagation();
    });
    $('.menu-close, .menu-svg, .menu-overlay').on('click', function () {
        $('html').toggleClass('menu-open');
    });
}
