from django.shortcuts import render
from django.db.models import Q
from moraiscanterosite.core.models import PaginaEstatica, Equipe, Artigo, Configuracao, Slider, AreasAtuacao
from django.shortcuts import render, get_object_or_404
from django.core.mail import send_mail



def home(request):
    sliders = Slider.objects.filter(tipo=1)
    config = Configuracao.objects.all()[:1]
    equipes = Equipe.objects.all().order_by('order')
    pagina = get_object_or_404(PaginaEstatica, tipo=1)
    context = {
        'config': config,
        'equipes': equipes,
        'sliders': sliders,
        'areas_atuacao': AreasAtuacao.objects.all(),
        'page_content': PaginaEstatica.objects.filter(tipo='1'),
        'seo_title': pagina.seo_title,
        'seo_description': pagina.seo_description,
        'seo_keywords': pagina.seo_keywords,
    }
    return render(request, 'index.html', context)


def sobre(request):
    config = Configuracao.objects.all()[:1]
    equipes = Equipe.objects.all().order_by('order')
    pagina = get_object_or_404(PaginaEstatica, tipo=1)
    context = {
        'config': config,
        'areas_atuacao': AreasAtuacao.objects.all(),
        'page_content': PaginaEstatica.objects.filter(tipo='2'),
        'equipes': equipes,
        'seo_title': pagina.seo_title,
        'seo_description': pagina.seo_description,
        'seo_keywords': pagina.seo_keywords,
    }
    return render(request, 'sobre.html', context)


def areas(request):
    config = Configuracao.objects.all()[:1]
    pagina = get_object_or_404(PaginaEstatica, tipo=3)
    context = {
        'config': config,
        'areas_atuacao': AreasAtuacao.objects.all(),
        'page_content': PaginaEstatica.objects.filter(tipo='3'),
        'seo_title': pagina.seo_title,
        'seo_description': pagina.seo_description,
        'seo_keywords': pagina.seo_keywords,
    }
    return render(request, 'areas.html', context)


def area_detalhe(request, slug):
    config = Configuracao.objects.all()[:1]
    area = get_object_or_404(AreasAtuacao, slug=slug)
    context = {
        'areas_atuacao': AreasAtuacao.objects.all(),
        'config': config,
        'titulo': area.titulo,
        'texto': area.texto,
        'image': area.image,
        'cropping': area.cropping,
        'seo_title': area.seo_title,
        'seo_description': area.seo_description,
        'seo_keywords': area.seo_keywords,
    }
    return render(request, 'area_detalhe.html', context)


def clientes(request):
    return render(request, 'clientes.html')


def artigos(request):
    config = Configuracao.objects.all()[:1]
    pagina = get_object_or_404(PaginaEstatica, tipo=5)
    context = {
        'config': config,
        'areas_atuacao': AreasAtuacao.objects.all(),
        'artigos': Artigo.objects.all(),
        'page_content': PaginaEstatica.objects.filter(tipo='5'),
        'seo_title': pagina.seo_title,
        'seo_description': pagina.seo_description,
        'seo_keywords': pagina.seo_keywords,
    }
    return render(request, 'artigos.html', context)


def artigo_detalhe(request, slug):
    config = Configuracao.objects.all()[:1]
    artigo = get_object_or_404(Artigo, slug=slug)
    ultimos_artigos = Artigo.objects.filter(~Q(id=artigo.id))[:2]
    context = {
        'areas_atuacao': AreasAtuacao.objects.all(),
        'config': config,
        'u_artigos': ultimos_artigos,
        'titulo': artigo.titulo,
        'sub_titulo': artigo.sub_titulo,
        'texto': artigo.texto,
        'image': artigo.image,
        'cropping': artigo.cropping,
        'data_publicacao': artigo.data_publicacao,
        'seo_title': artigo.seo_title,
        'seo_description': artigo.seo_description,
        'seo_keywords': artigo.seo_keywords,
    }
    return render(request, 'artigo_detalhe.html', context)


def equipe(request):
    config = Configuracao.objects.all()[:1]
    pagina = get_object_or_404(PaginaEstatica, tipo=7)
    sliders = Slider.objects.filter(tipo=2)
    equipes = Equipe.objects.all().order_by('order')
    context = {
        'config': config,
        'areas_atuacao': AreasAtuacao.objects.all(),
        'page_content': PaginaEstatica.objects.filter(tipo='7'),
        'equipes': equipes,
        'seo_title': pagina.seo_title,
        'seo_description': pagina.seo_description,
        'seo_keywords': pagina.seo_keywords,
        'sliders': sliders,
    }
    return render(request, 'equipe.html', context)


def equipe_detalhe(request, slug):
    config = Configuracao.objects.all()[:1]
    equipe = get_object_or_404(Equipe, slug=slug)
    context = {
        'areas_atuacao': AreasAtuacao.objects.all(),
        'config': config,
        'titulo': equipe.titulo,
        'texto': equipe.texto,
        'image': equipe.image,
        'cropping': equipe.cropping,
        'facebook': equipe.facebook,
        'instagram': equipe.instagram,
        'linkedin': equipe.linkedin,
        'seo_title': equipe.seo_title,
        'seo_description': equipe.seo_description,
        'seo_keywords': equipe.seo_keywords,
    }
    return render(request, 'equipe_detalhe.html', context)


def contato(request):
    config = Configuracao.objects.all()[:1]
    pagina = get_object_or_404(PaginaEstatica, tipo=6)
    context = {
        'config': config,
        'artigos': Artigo.objects.all(),
        'page_content': PaginaEstatica.objects.filter(tipo='6'),
        'seo_title': pagina.seo_title,
        'seo_description': pagina.seo_description,
        'seo_keywords': pagina.seo_keywords,
        'areas_atuacao': AreasAtuacao.objects.all(),
    }
    return render(request, 'contato.html', context)


'''
    FORM CONTATO MENSAGEM
    Nome, email, telefone, assunto e mensagem
'''


def contact_form(request):
    if request.method == 'POST':
        nome = request.POST.get('nome')
        email = request.POST.get('email')
        telefone = request.POST.get('telefone')
        assunto = request.POST.get('assunto')
        mensagem_form = request.POST.get('mensagem')

    from_email = "administrador@moraiscantero.com.br"
    to_email = "diegohfranco@gmail.com"

    send_mail(assunto, mensagem_form, from_email, [to_email],)

    context = {
        'resposta': 'ok',
    }
    return render(request, 'envio-email.html', context)