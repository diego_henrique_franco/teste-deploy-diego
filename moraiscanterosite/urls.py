"""moraiscanterosite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from moraiscanterosite.core.views import home, sobre, areas, clientes, artigos, contato, artigo_detalhe, \
    area_detalhe, equipe_detalhe, equipe, contact_form
from django.conf.urls import include, url
from django.contrib import admin
from django.conf.urls.static import static
from django.conf import settings



urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', home, name='home'),
    url(r'^sobre/', sobre, name='sobre'),
    url(r'^areas-de-atuacao/$', areas, name='areas'),
    url(r'^areas-de-atuacao/(?P<slug>[\w-]+)/$', area_detalhe, name='area_detalhe'),
    url(r'^clientes/', clientes, name='clientes'),
    url(r'^artigos/$', artigos, name='artigos'),
    url(r'^artigos/(?P<slug>[\w-]+)/$', artigo_detalhe, name='artigo_detalhe'),
    url(r'^equipe/$', equipe, name='equipe'),
    url(r'^equipe/(?P<slug>[\w-]+)/$', equipe_detalhe, name='equipe_detalhe'),
    url(r'^contato/', contato, name='contato'),
    url(r'^contact_form', contact_form, name='contact_form'),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

